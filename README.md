# AppState Writing Template

This repo contains a script that generates a [Pandoc][0]-ready [Markdown][1]
document. This document is compliant with all the formatting and writing
guidelines specified by the Appalachian State University Department of Computer
Science.

## Usage

`genpaper document.md`

In this example, `genpaper` is the executable and `document.md` is the name of
the output file. The program will not allow 2 or more files. Likewise, providing
no file name will also produce an error.

## Installing

1. Clone the repo:
`$ git clone https://gitlab.com/caton101/appstate-cs-writing-template.git`
2. Enter the directory:
`$ cd appstate-cs-writing-template`
3. Mark the install script as an executable:
`$ chmod +x install-global.sh`
4. Execute the install script:
`$ ./install-global.sh`

All together as one block:

```sh
git clone https://gitlab.com/caton101/appstate-cs-writing-template.git
cd appstate-cs-writing-template
chmod +x install-global.sh
./install-global.sh
```

**NOTE**: If you do not have sudo access or do not want to install for all
users, use `install-user.sh` instead of `install-global.sh`.

## Uninstalling

1. Clone the repo:
`$ git clone https://gitlab.com/caton101/appstate-cs-writing-template.git`
2. Enter the directory:
`$ cd appstate-cs-writing-template`
3. Mark the uninstall script as an executable:
`$ chmod +x uninstall-global.sh`
4. Execute the uninstall script:
`$ ./uninstall-global.sh`

All together as one block:

```sh
git clone https://gitlab.com/caton101/appstate-cs-writing-template.git
cd appstate-cs-writing-template
chmod +x uninstall-global.sh
./uninstall-global.sh
```

**NOTE**: If you installed for only your user, use `uninstall-user.sh` instead
of `uninstall-global.sh`.

[0]: https://pandoc.org/
[1]: https://garrettgman.github.io/rmarkdown/authoring_pandoc_markdown.html
