---
geometry: margin=1in
papersize: letter
fontsize: 12pt
linestretch: 2
output: pdf_document
lang: english
header-includes: |
    \thispagestyle{empty}
    \usepackage{indentfirst}
    \usepackage[all]{nowidow}
    \raggedright
---

First Last

Dr. First Last

C S 1234

January 1, 1970

\centering

My Title

\raggedright
\parindent=0.5in

<!-- write your paper below this line -->

This is the first paragraph in the paper. Don't forget to change the header of
your paper from the default text listed above. Anything inside `---` should be
left alone since it is used to apply formatting to the page. In addition, do not
touch anything above this paragraph that starts with `\`. These are LaTeX
commands which help to format this paper. The `\center` command will center the
title. The `\raggedright` command makes the text left justified. The
`\parindent=0.5in` command sets up indentation for the rest of the document.
Lastly, you can generate your PDF file with `pandoc -f markdown
--pdf-engine=lualatex document.md -o document.pdf`. Have fun!

Alternatively, you can copy the following code into a `makefile` and use the
`make` command to generate your paper. It is much faster to type `make` than
that long Pandoc command above. 

```sh
paper:
	pandoc -f markdown --pdf-engine=lualatex paper.md -o paper.pdf
```

<!-- write your bibliography below this line -->

<!--
I know this might look confusing but it is not. Just make sure `\bibitem{name}`
is unique and use it in your paper with `\cite{name}`. Be sure to put your URLs
inside a LaTeX tag when making your BibLaTeX bibliography. A URL in this format
would look like `\url{https://www.code-scribe.com}` instead of writing the URL
in normal Markdown.

See this page for more information:
https://www.overleaf.com/learn/latex/Bibliography_management_with_bibtex
-->

\begin{thebibliography}{1}
\bibliographystyle{plain}

\bibitem{name}
H. Cameron, “Code Scribe is The Best Website,” Code Scribe, 18-Nov-2021.
[Online]. Available: \url{https://www.code-scribe.com}. [Accessed: 18-Nov-2021].

\end{thebibliography}
