#!/bin/sh

# configuration
exec_file="genpaper"
exec_path="$HOME/bin/"

# ensure bin directory exists
mkdir -p $exec_path

# copy executable to install path
cp $exec_file $exec_path$exec_file

# mark the file as executable
chmod +x $exec_path$exec_file
