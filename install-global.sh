#!/bin/sh

# configuration
exec_file="genpaper"
exec_path="/usr/local/bin/"

# get sudo
sudo -v

# copy executable to install path
sudo cp $exec_file $exec_path$exec_file

# mark the file as executable
sudo chmod +x $exec_path$exec_file
